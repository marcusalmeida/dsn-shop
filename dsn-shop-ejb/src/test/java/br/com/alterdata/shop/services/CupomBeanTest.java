package br.com.alterdata.shop.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.alterdata.shop.entities.Cupom;
import br.com.alterdata.shop.entities.ItemCupom;
import br.com.alterdata.shop.entities.Produto;
import junit.framework.TestCase;

@RunWith(Arquillian.class)
public class CupomBeanTest extends TestCase {
	
	private static long createdId = 0;

	@Deployment
    public static Archive<?> createDeployment() {
		 WebArchive war = ShrinkWrap.create(WebArchive.class, "cupom-test.war")
				 .addPackages(true, "br.com.alterdata.shop.dao")
				 .addPackage(Cupom.class.getPackage())
				 .addClass(CupomBean.class)
				 .addClass(ProdutoBean.class)
				 .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				 .addAsWebInfResource("META-INF/test-ds.xml", "test-ds.xml")
				 .addAsWebInfResource(EmptyAsset.INSTANCE,"beans.xml");
		 return war;
    }
	
	@EJB
	CupomBean bean;
	
	@EJB
	ProdutoBean produtoBean;
	
	@Test
	@InSequence(1)
	public void deveriaSalvarUmCupom() throws Exception {
		Produto produto = new Produto();
		produto.setNome("PROD");
		produto.setValor(new BigDecimal("12.00"));
		produtoBean.criarProduto(produto);
		
		Cupom cupom = new Cupom();
		cupom.setDataHoraEmissao(new Date(System.currentTimeMillis()));
		ItemCupom item = new ItemCupom();
		item.setProduto(produto);
		item.setQuantidade(1);
		item.setValor(produto.getValor());
		List<ItemCupom> itens = new ArrayList<>();
		itens.add(item);
		cupom.setItens(itens);
		
		cupom = bean.criarCupom(cupom);
		 
		assertNotNull(cupom.getId());
		createdId = cupom.getId();
	}
	
	@Test
	@InSequence(2)
	public void deveriaListarTodosOsCuponsCriados() throws Exception {
		List<Cupom> cupons = bean.listarTodosCupoms();
		
		assertFalse(cupons.isEmpty());
		assertEquals(1, cupons.size());
	}
	
	@Test
	@InSequence(3)
	public void deveriaBuscarUmCupomPorId() throws Exception {
		Cupom cupom = bean.buscarPorId(createdId);
		
		assertNotNull(cupom.getId());
		assertEquals(createdId, cupom.getId().longValue());
	}
	
	@Test
	@InSequence(4)
	public void deveriaAtualizarUmCupom() throws Exception {
		Cupom cupom = bean.buscarPorId(createdId);
		cupom.setDataHoraEmissao(new Date(System.currentTimeMillis()));
		
		Cupom cupomAtualizado = bean.atualizarCupom(cupom);
		
		assertNotNull(cupomAtualizado.getId());
		assertEquals(cupom.getDataHoraEmissao(), cupomAtualizado.getDataHoraEmissao());
	}
	
	@Test
	@InSequence(5)
	public void deveriaApagarUmCupom() throws Exception {
		bean.excluirCupom(createdId);
		
		Cupom cupom = bean.buscarPorId(createdId);
		assertNull(cupom);
	}
}
