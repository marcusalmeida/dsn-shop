package br.com.alterdata.shop.services;

import static org.junit.Assert.assertNotEquals;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.alterdata.shop.entities.Produto;
import junit.framework.TestCase;

@RunWith(Arquillian.class)
public class ProdutoBeanTest extends TestCase {
	private static long createdId = 0;
    private static String PRODUCT_NAME = "PROD 1";
	private static BigDecimal PRODUCT_PRICE = new BigDecimal("11.10");
	
	@Deployment
    public static Archive<?> createDeployment() {
		 WebArchive war = ShrinkWrap.create(WebArchive.class, "produto-test.war")
				 .addPackages(true, "br.com.alterdata.shop.dao")
				 .addPackage(Produto.class.getPackage())
				 .addClass(ProdutoBean.class)
				 .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				 .addAsWebInfResource("META-INF/test-ds.xml", "test-ds.xml")
				 .addAsWebInfResource(EmptyAsset.INSTANCE,"beans.xml");
		 return war;
    }
	
	@EJB
	ProdutoBean bean;
	
	@Test
	@InSequence(1)
	public void deveriaInserirProduto() throws Exception {
		 Produto p = new Produto();
		 p.setNome(PRODUCT_NAME);
		 p.setValor(PRODUCT_PRICE);
		 p = bean.criarProduto(p);
		 
		 assertNotNull(p.getId());
		 createdId = p.getId();
	}
	
	@Test(expected=Exception.class)
	@InSequence(2)
	public void naoDeveriaPermitirCriarProdutoComoMesmoNome() {
		 Produto p = new Produto();
		 p.setNome(PRODUCT_NAME);
		 p.setValor(PRODUCT_PRICE);
		 p = bean.criarProduto(p);
	}
	
	@Test
	@InSequence(3)
	public void deveriaBuscarProdutoPorId() throws Exception {
		Produto p = bean.buscarPorId(createdId);
		assertNotNull(p);
		assertEquals(PRODUCT_NAME, p.getNome());
		assertEquals(PRODUCT_PRICE, p.getValor());
	}
	
	@Test
	@InSequence(4)
	public void deveriaAtualizarProduto() throws Exception {
		Produto p = bean.buscarPorId(createdId);
		p.setNome("PROD 2");
		
		p = bean.atualizarProduto(p);
		
		assertNotNull(p);
		assertEquals(createdId, p.getId().longValue());
		assertEquals("PROD 2", p.getNome());
		
	}
	
	@Test
	@InSequence(5)
	public void deveriaApagarProduto() throws Exception {
		bean.excluirProduto(createdId);
		
		Produto p = bean.buscarPorId(createdId);
		assertNull(p);
	}
	
	@Test
	@InSequence(6)
	public void deveriaRecuperarTodosProdutos() throws Exception {
		for (int i=0; i<10; i++) {
			Produto p = new Produto();
			p.setNome(PRODUCT_NAME + " " + i);
			p.setValor(PRODUCT_PRICE);
			bean.criarProduto(p);
		}
		List<Produto> produtos = bean.listarTodosProdutos();
		assertNotEquals(0, produtos.size());
		assertEquals(10, produtos.size());
	}
}
