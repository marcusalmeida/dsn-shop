package br.com.alterdata.shop.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.alterdata.shop.dao.CupomDAO;
import br.com.alterdata.shop.entities.Cupom;
import br.com.alterdata.shop.entities.ItemCupom;

public class CupomDAOImpl implements CupomDAO {
	
	private EntityManager em;
	
	public CupomDAOImpl(EntityManager em) {
		this.em = em;
	}

	@Override
	public List<Cupom> listarTodos() {
		TypedQuery<Cupom> query = em.createNamedQuery(Cupom.LISTAR_TODOS, Cupom.class);
	    List<Cupom> cupons =  query.getResultList();
	    return cupons == null ? new ArrayList<>() : cupons;
	}
	
	@Override
	public Cupom inserir(Cupom c) {
		em.persist(c);
		for (ItemCupom item : c.getItens()) {
			item.setCupom(c);
		}
		return c;
	}
	
	@Override
	public Cupom atualizar(Cupom c) {
		for (ItemCupom item : c.getItens()) {
			item.setCupom(c);
			em.merge(item);
		}
		return em.merge(c);
	}
	
	@Override
	public void excluir(Long id) {
		em.remove(em.find(Cupom.class, id));
	}
	
	@Override
	public Cupom buscarPorId(Long id) {
		return em.find(Cupom.class, id);
	}
}
