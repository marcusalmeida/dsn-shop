package br.com.alterdata.shop.entities;

import java.util.Date;
import java.util.List;

import javax.json.bind.annotation.JsonbDateFormat;
import javax.json.bind.annotation.JsonbProperty;
import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="cupom")
@AttributeOverride(name = "id", column = @Column(name = "id_cupom", nullable = false))
@NamedQueries({
	@NamedQuery(name = Cupom.LISTAR_TODOS, query="SELECT c from Cupom c")
})
public class Cupom extends BaseEntity<Long> {
	
	private static final long serialVersionUID = 1L;
	
	public static final String LISTAR_TODOS = "Cupom.listarTodos";
	
	@NotNull
	@Column(name="dh_emissao", nullable = false, columnDefinition = "TIMESTAMP")
	@JsonbProperty("data-hora-emissao")
	@JsonbDateFormat("yyyy-MM-dd HH:mm")
	private Date dataHoraEmissao;
	
	@OneToMany(mappedBy= "cupom", fetch = FetchType.EAGER,
			  targetEntity = ItemCupom.class,
			  cascade=CascadeType.ALL, 
			  orphanRemoval = true)
	@JsonbProperty("itens")
	private List<ItemCupom> itens;
	
	public Cupom() {}

	public Date getDataHoraEmissao() {
		return dataHoraEmissao;
	}
	
	public void setDataHoraEmissao(Date dataHoraEmissao) {
		this.dataHoraEmissao = dataHoraEmissao;
	}

	public List<ItemCupom> getItens() {
		return itens;
	}

	public void setItens(List<ItemCupom> itens) {
		this.itens = itens;
	}
}