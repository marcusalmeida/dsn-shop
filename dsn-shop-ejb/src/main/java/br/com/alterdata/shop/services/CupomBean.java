package br.com.alterdata.shop.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.alterdata.shop.dao.CupomDAO;
import br.com.alterdata.shop.entities.Cupom;

@Stateless
public class CupomBean {
	
	@Inject
	private CupomDAO dao;
	
	public Cupom criarCupom(Cupom c) {
		return dao.inserir(c);
	}
	
	public Cupom atualizarCupom(Cupom c) {
		return dao.atualizar(c);
	}
	
	public void excluirCupom(Long id) {
		dao.excluir(id);
	}
	
	public List<Cupom> listarTodosCupoms() {
		return dao.listarTodos();
	}
	
	public Cupom buscarPorId(Long id) {
		Cupom cupom =  dao.buscarPorId(id);
		return cupom;
	}
}
