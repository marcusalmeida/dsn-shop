package br.com.alterdata.shop.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.alterdata.shop.dao.ProdutoDAO;
import br.com.alterdata.shop.entities.Produto;

@Stateless
public class ProdutoBean {
	
	@Inject
	private ProdutoDAO dao;
	
	public Produto criarProduto(Produto p) {
		return dao.inserir(p);
	}
	
	public Produto atualizarProduto(Produto p) {
		return dao.atualizar(p);
	}
	
	public void excluirProduto(Long id) {
		dao.excluir(id);
	}
	
	public List<Produto> listarTodosProdutos() {
		return dao.listarTodos();
	}
	
	public Produto buscarPorId(Long id) {
		return dao.buscarPorId(id);
	}
}
