package br.com.alterdata.shop.dao;

import java.util.List;

import br.com.alterdata.shop.entities.Cupom;

public interface CupomDAO {
	Cupom inserir(Cupom  c);
	Cupom atualizar(Cupom c);
	void excluir(Long id);
	Cupom buscarPorId(Long id);
	List<Cupom> listarTodos();
}
