package br.com.alterdata.shop.entities;

import java.math.BigDecimal;

import javax.json.bind.annotation.JsonbNumberFormat;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "item_cupom")
@AttributeOverride(name = "id", column = @Column(name = "id_item_cupom", nullable = false))
public class ItemCupom extends BaseEntity<Long> {

	private static final long serialVersionUID = 1L;
	
	@NotNull
	@Min(value = 1L)
	@Column(name = "quantidade", nullable = false)
	private Integer quantidade;

	@Column(name = "valor", nullable = false, precision = 12, scale=2)
	@JsonbNumberFormat(locale = "pt_BR", value = "#0.00")
	private BigDecimal valor;

	@ManyToOne
	@JoinColumn(name = "id_produto", 
	            foreignKey = @ForeignKey(value = ConstraintMode.CONSTRAINT, name = "FK_ID_PRODUTO"))
	private Produto produto;	
	
	@ManyToOne
	@JoinColumn(name = "id_cupom", 
	            foreignKey = @ForeignKey(value = ConstraintMode.CONSTRAINT, name = "FK_ID_CUPOM"))
	@JsonbTransient
	private Cupom cupom;

	public ItemCupom() {
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Cupom getCupom() {
		return cupom;
	}

	public void setCupom(Cupom cupom) {
		this.cupom = cupom;
	}

}
