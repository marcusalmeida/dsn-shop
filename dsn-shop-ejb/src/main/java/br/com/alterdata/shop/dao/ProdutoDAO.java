package br.com.alterdata.shop.dao;

import java.util.List;

import br.com.alterdata.shop.entities.Produto;

public interface ProdutoDAO {
	Produto inserir(Produto  p);
	Produto atualizar(Produto p);
	void excluir(Long id);
	Produto buscarPorId(Long id);
	List<Produto> listarTodos();
}
