package br.com.alterdata.shop.entities;

import java.math.BigDecimal;

import javax.json.bind.annotation.JsonbNumberFormat;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="produto",
	   uniqueConstraints = @UniqueConstraint(columnNames = { "nome" }, name="PROD_NAME_UNIQ"))
@AttributeOverride(name = "id", column = @Column(name = "id_produto", nullable = false))
@NamedQueries({
	@NamedQuery(name = Produto.LISTAR_TODOS, query="SELECT p from Produto p")
})
public class Produto extends BaseEntity<Long> {

	private static final long serialVersionUID = 1L;
	
	public static final String LISTAR_TODOS = "Produto.listarTodos";
	
	@NotNull
	@NotBlank
	@Column(name="nome", nullable = false, unique = true)
	private String nome;
	
	@NotNull
	@Column(name="valor", nullable = false, precision = 12, scale=2)
	@JsonbNumberFormat(locale = "pt_BR", value = "#0.00")
	private BigDecimal valor;
	
	public Produto() {}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
}