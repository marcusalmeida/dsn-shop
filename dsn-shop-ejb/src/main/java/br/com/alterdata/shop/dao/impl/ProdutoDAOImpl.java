package br.com.alterdata.shop.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.alterdata.shop.dao.ProdutoDAO;
import br.com.alterdata.shop.entities.Produto;

public class ProdutoDAOImpl implements ProdutoDAO {
	
	private EntityManager em;
	
	public ProdutoDAOImpl(EntityManager em) {
		this.em = em;
	}

	@Override
	public List<Produto> listarTodos() {
		TypedQuery<Produto> query = em.createNamedQuery(Produto.LISTAR_TODOS, Produto.class);
	    List<Produto> produtos =  query.getResultList();
	    return produtos == null ? new ArrayList<>() : produtos;
	}
	
	@Override
	public Produto inserir(Produto p) {
		em.persist(p);
		return p;
	}
	
	@Override
	public Produto atualizar(Produto p) {
		return em.merge(p);
	}
	
	@Override
	public void excluir(Long id) {
		em.remove(em.find(Produto.class, id));
	}
	
	@Override
	public Produto buscarPorId(Long id) {
		return em.find(Produto.class, id);
	}
}
