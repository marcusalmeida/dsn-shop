package br.com.alterdata.shop.dao.factory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.alterdata.shop.dao.CupomDAO;
import br.com.alterdata.shop.dao.ProdutoDAO;
import br.com.alterdata.shop.dao.impl.CupomDAOImpl;
import br.com.alterdata.shop.dao.impl.ProdutoDAOImpl;

@ApplicationScoped
public class DAOFactory {
	
	@PersistenceContext(unitName = "shop-jpa-pu")
	private EntityManager em;
	
	@Produces
	public ProdutoDAO createProductDAO() {
		return new ProdutoDAOImpl(em);
	}
	
	@Produces
	public CupomDAO createCupomDAO() {
		return new CupomDAOImpl(em);
	}
}
