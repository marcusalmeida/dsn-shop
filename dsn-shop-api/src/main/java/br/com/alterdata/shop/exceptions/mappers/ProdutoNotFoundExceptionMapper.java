package br.com.alterdata.shop.exceptions.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import br.com.alterdata.shop.exceptions.ProdutoNotFoundException;

@Provider
public class ProdutoNotFoundExceptionMapper 
	implements ExceptionMapper<ProdutoNotFoundException>  {

	@Override
	public Response toResponse(ProdutoNotFoundException exception) {
		return Response
				.status(Response.Status.NOT_FOUND)
				.entity(exception.getMessage())
				.build();
	}

}
