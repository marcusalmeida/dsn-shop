package br.com.alterdata.shop.exceptions;

public class CupomNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public CupomNotFoundException(Long id) {
		super("Cupom com id="+ id + " não encontrado.");
	}
}