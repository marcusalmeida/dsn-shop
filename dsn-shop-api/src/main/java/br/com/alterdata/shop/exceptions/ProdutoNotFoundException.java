package br.com.alterdata.shop.exceptions;

public class ProdutoNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ProdutoNotFoundException(Long id) {
		super("Produto com id="+ id + " não encontrado.");
	}
}
