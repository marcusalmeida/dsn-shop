package br.com.alterdata.shop.resources;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.alterdata.shop.entities.Produto;
import br.com.alterdata.shop.exceptions.ProdutoNotFoundException;
import br.com.alterdata.shop.services.ProdutoBean;

@Path("/produtos")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class ProdutoResource {
	
	@Inject
	private ProdutoBean produtoBean;
	
	@GET
	public Response listarTodosProdutos() {
		List<Produto> produtos = produtoBean.listarTodosProdutos();
		if (produtos.isEmpty()) {			
			return Response.status(Response.Status.NOT_FOUND).build();
		} else {
			return Response.ok(produtos).build();
		}
	}
	
	@GET
	@Path("{id}")
	public Response buscarPorId(@PathParam("id")Long id) {
		Produto produto = produtoBean.buscarPorId(id);
		if(produto == null) {
			throw new ProdutoNotFoundException(id);
		}
		return Response.ok(produto).build();	
	}
	
	@POST
	public Response criarProduto(@Valid Produto produto) {
		Produto produtoCriado = produtoBean.criarProduto(produto);
		return Response.status(Status.CREATED).entity(produtoCriado).build();
	}
	
	@PUT
	@Path("{id}")
	public Response atualizarProduto(@PathParam("id")Long id, @Valid Produto update) {
		Produto produto = produtoBean.buscarPorId(id);
		if(produto == null) {
			throw new ProdutoNotFoundException(id);
		}
		if(update.getNome() != null) {			
			produto.setNome(update.getNome());
		}
		if(update.getValor() != null) {			
			produto.setValor(update.getValor());
		}
		return Response.ok(produtoBean.atualizarProduto(produto)).build();
	}
	
	@DELETE
	@Path("{id}")
	public Response apagarProduto(@PathParam("id") Long id) {
		Produto produto = produtoBean.buscarPorId(id);
		if(produto == null) {
			throw new ProdutoNotFoundException(id);
		}
		produtoBean.excluirProduto(id);
		return Response.status(Status.NO_CONTENT).build();
	}
	
}