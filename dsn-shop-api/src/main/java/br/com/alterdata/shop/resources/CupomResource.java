package br.com.alterdata.shop.resources;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.alterdata.shop.entities.Cupom;
import br.com.alterdata.shop.exceptions.CupomNotFoundException;
import br.com.alterdata.shop.services.CupomBean;

@Path("/cupons")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class CupomResource {
	
	@Inject
	private CupomBean bean;
	
	@GET
	public Response listarTodosCupons() {
		List<Cupom> cupons = bean.listarTodosCupoms();
		if (cupons.isEmpty()) {
			return Response.status(Response.Status.NOT_FOUND).build();
		} else {			
			return Response.ok(cupons).build();
		}
	}
	
	@GET
	@Path("{id}")
	public Response buscarPorId(@PathParam("id")Long id) {
		Cupom cupom = bean.buscarPorId(id);
		if(cupom == null) {
			throw new CupomNotFoundException(id);
		}
		return Response.ok(cupom).build();	
	}
	
	@POST
	public Response criarCupom(@Valid Cupom cupom) {
		Cupom cupomCriado = bean.criarCupom(cupom);
		return Response.status(Status.CREATED).entity(cupomCriado).build();
	}
	
	@PUT
	@Path("{id}")
	public Response atualizarCupom(@PathParam("id") Long id, Cupom update) {
		Cupom cupom = bean.buscarPorId(id);
		if (cupom == null) {
			throw new CupomNotFoundException(id);
		}
		cupom.setDataHoraEmissao(update.getDataHoraEmissao());
		cupom.setItens(update.getItens());
		return Response.ok(bean.atualizarCupom(cupom)).build();
	}
	
	@DELETE
	@Path("{id}")
	public Response excluirCupom(@PathParam("id") Long id) {
		Cupom cupom = bean.buscarPorId(id);
		if (cupom == null) {
			throw new CupomNotFoundException(id);
		}
		bean.excluirCupom(id);
		return Response.status(Status.NO_CONTENT).build();
	}
}
