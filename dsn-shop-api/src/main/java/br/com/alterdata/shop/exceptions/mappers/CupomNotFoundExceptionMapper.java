package br.com.alterdata.shop.exceptions.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import br.com.alterdata.shop.exceptions.CupomNotFoundException;

@Provider
public class CupomNotFoundExceptionMapper 
	implements ExceptionMapper<CupomNotFoundException>  {

	@Override
	public Response toResponse(CupomNotFoundException exception) {
		return Response
				.status(Response.Status.NOT_FOUND)
				.entity(exception.getMessage())
				.build();
	}
}
