package br.com.alterdata.shop.resources;

import java.math.BigDecimal;
import java.util.List;

import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.alterdata.shop.entities.Produto;
import junit.framework.TestCase;

@RunWith(Arquillian.class)
public class ProdutoResourceTest extends TestCase {
	
	@Deployment
    public static Archive<?> createDeployment() {
		 WebArchive war = ShrinkWrap.create(WebArchive.class, "produto-rest-test.war")
				 .addPackages(true, "br.com.alterdata.shop")
				 .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				 .addAsWebInfResource("META-INF/test-ds.xml", "test-ds.xml")
				 .addAsWebInfResource(EmptyAsset.INSTANCE,"beans.xml");
		 return war;
    }
		
	@Test
	@InSequence(1)
	public void deveriaRetornar200SalvandoProduto(
			@ArquillianResteasyResource ProdutoResource resource) throws Exception {
		Produto produto = new Produto();
		produto.setNome("PROD");
		produto.setValor(new BigDecimal("12.00"));
		Response response = resource.criarProduto(produto);
		
		Produto produtoSalvo = (Produto) response.getEntity();
		assertNotNull(produtoSalvo.getId());
		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
	}
	
	@Test
	@InSequence(2)
	public void deveriaRetornarUmProdutoQuandoBuscarPorId(
			@ArquillianResteasyResource ProdutoResource resource) throws Exception {
		Response response = resource.buscarPorId(1L);
		
		Produto produto = (Produto) response.getEntity();
		assertEquals(1L, produto.getId().longValue());
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	}
	
	@Test
	@InSequence(3)
	public void deveriaAtualizarUmProduto(
			@ArquillianResteasyResource ProdutoResource resource) throws Exception {
		Produto update = new Produto();
		update.setNome("PROD 2");
		update.setValor(new BigDecimal("10.00"));
		Response response = resource.atualizarProduto(1L, update);
		
		Produto produtoAtualizado = (Produto) response.getEntity();
		assertEquals(update.getNome(), produtoAtualizado.getNome());
		assertEquals(update.getValor(), produtoAtualizado.getValor());
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	}
	
	
	@Test
	@InSequence(4)
	public void deveriaRetornarTodosOsProdutos(
			@ArquillianResteasyResource ProdutoResource resource) throws Exception {
		Response response = resource.listarTodosProdutos();
		
		@SuppressWarnings("unchecked")
		List<Produto> produtos = (List<Produto>) response.getEntity();
		
		assertFalse(produtos.isEmpty());
		assertEquals(1, produtos.size());
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	}
	
	@Test
	@InSequence(5)
	public void deveriaApagarOProduto(
			@ArquillianResteasyResource ProdutoResource resource) throws Exception {
		Response response = resource.apagarProduto(1L);
		assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response.getStatus());
	}
	
	@Test
	@InSequence(6)
	public void deveriaSemProdutosRetornar404QuandoListarTodosOsProdutos(
			@ArquillianResteasyResource ProdutoResource resource) throws Exception {
		Response response = resource.listarTodosProdutos();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	@Test(expected = Exception.class)
	@InSequence(7)
	public void deveriaRetornar400QuandoProdutoInvalido(
			@ArquillianResteasyResource ProdutoResource resource) throws Exception {
		Produto produto = new Produto();
		produto.setValor(new BigDecimal("12.00"));
		Response response = resource.criarProduto(produto);
		
		assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}
}
