package br.com.alterdata.shop.resources;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.alterdata.shop.entities.Cupom;
import br.com.alterdata.shop.entities.ItemCupom;
import br.com.alterdata.shop.entities.Produto;
import br.com.alterdata.shop.services.CupomBean;
import br.com.alterdata.shop.services.ProdutoBean;
import junit.framework.TestCase;

@RunWith(Arquillian.class)
public class CupomResourceTest extends TestCase {

	@Deployment
    public static Archive<?> createDeployment() {
		 WebArchive war = ShrinkWrap.create(WebArchive.class, "cupom-rest-test.war")
				 .addPackages(true, "br.com.alterdata.shop")
				 .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				 .addAsWebInfResource("META-INF/test-ds.xml", "test-ds.xml")
				 .addAsWebInfResource(EmptyAsset.INSTANCE,"beans.xml");
		 return war;
    }
	
	@Inject
	ProdutoBean produtoBean;
	
	@Inject
	CupomBean cupomBean;
	
	private Produto produto1;
	private Cupom cupom;
	
	@Test
	@InSequence(1)
	public void deveriaRetornar200SalvandoCupom(
			@ArquillianResteasyResource CupomResource resource) throws Exception {
		produto1 = salvarProduto("PROD 1", new BigDecimal("12.00"));
		
		cupom = new Cupom();
		cupom.setDataHoraEmissao(new Date(System.currentTimeMillis()));
		ItemCupom item = new ItemCupom();
		item.setProduto(produto1);
		item.setQuantidade(1);
		item.setValor(produto1.getValor());
		List<ItemCupom> itens = new ArrayList<>();
		itens.add(item);
		cupom.setItens(itens);
		
		Response response = resource.criarCupom(cupom);
		
		Cupom cupomSalvo = (Cupom) response.getEntity();
		assertNotNull(cupomSalvo.getId());
		assertEquals(cupom.getDataHoraEmissao(), cupomSalvo.getDataHoraEmissao());
		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
	}
	
	@Test
	@InSequence(2)
	public void deveriaRetornarTodosOsCupons(
			@ArquillianResteasyResource CupomResource resource) throws Exception {
		Response response = resource.listarTodosCupons();
		
		@SuppressWarnings("unchecked")
		List<Cupom> cupons = (List<Cupom>) response.getEntity();		
		assertFalse(cupons.isEmpty());
		assertEquals(1, cupons.size());
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	}
	
	@Test
	@InSequence(3)
	public void deveriaRetornarUmCupomQuandoBuscarPorId(
			@ArquillianResteasyResource CupomResource resource) throws Exception {
		Response response = resource.buscarPorId(1L);
		
		Cupom cupom = (Cupom) response.getEntity();
		assertEquals(1L, cupom.getId().longValue());
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	}
	
	@Test
	@InSequence(4)
	public void deveriaAtualizarUmCupom(
			@ArquillianResteasyResource CupomResource resource) throws Exception {
		Cupom cupom = cupomBean.buscarPorId(1L);
		cupom.setDataHoraEmissao(new Date(System.currentTimeMillis()));
		
		Response response = resource.atualizarCupom(1L, cupom);
		Cupom cupomAtualizado = (Cupom) response.getEntity();
		assertNotNull(cupomAtualizado.getId());
		assertEquals(cupom.getDataHoraEmissao(), cupomAtualizado.getDataHoraEmissao());
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	}
	
	@Test
	@InSequence(5)
	public void deveriaApagarUmCupom(
			@ArquillianResteasyResource CupomResource resource) throws Exception {
		Response response = resource.excluirCupom(1L);
		assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response.getStatus());
	}
	
	private Produto salvarProduto(String nome, BigDecimal valor) {
		Produto produto = new Produto();
		produto.setNome(nome);
		produto.setValor(valor);
		return produtoBean.criarProduto(produto);
	}
}
